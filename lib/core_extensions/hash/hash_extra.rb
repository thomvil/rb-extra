# frozen_string_literal: true

module CoreExtensions
  module Hash
    # Common helpers for Hash
    module HashExtra
      def refresh(other_hash)
        merge(other_hash.slice(*keys))
      end

      def partial_match?(other_hash)
        other_hash.reduce(true) { |acc, (key, val)| acc && self[key] == val }
      end

      def map_values
        map { |key, val| [key, yield(key, val)] }.to_h
      end

      def map_keys
        map { |key, val| [yield(key, val), val] }.to_h
      end

      def symbolize_keys
        map_keys { |key, _val| key.to_sym }
      end

      def stringify_keys
        map_keys { |key, _val| key.to_s }
      end
    end
  end
end

Hash.include CoreExtensions::Hash::HashExtra
