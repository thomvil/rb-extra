# frozen_string_literal: true

require 'matrix'
require_relative 'data_points'
require_relative 'parabola'

# :reek:UtilityFunction
module CoreExtensions
  module Math
    # Math helpers
    module MathExtra
      def regress(datapoints, degree)
        x_data = datapoints.x_vals.map { |el| running_powers el, degree }
        mat_x = Matrix[*x_data]
        mat_y = Matrix.column_vector(datapoints.y_vals)
        least_squares mat_x, mat_y
      end

      def parabolic_opt(datapoints)
        Parabola.new(*regress(datapoints, 2)).opt
      end

      def moore_penrose(mat)
        mat_t = mat.t
        (mat_t * mat).inv * mat_t
      end

      def least_squares(mat_x, mat_y)
        (moore_penrose(mat_x) * mat_y).t.to_a[0].reverse
      end

      def running_powers(el, degree)
        (0..degree).map { |pow| el.to_f**pow }
      end

      def fix_to_range(val, range)
        [[val, range.min].max, range.max].min
      end
    end
  end
end

Math.extend CoreExtensions::Math::MathExtra
