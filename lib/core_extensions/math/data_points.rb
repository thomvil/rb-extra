# frozen_string_literal: true

require_relative 'parabola'

# struct for datapoints
DataPoints = Struct.new(:x_vals, :y_vals) do
  def self.init(x_vals, y_vals)
    new(x_vals, y_vals).dim!
  end

  def dim!
    x_len = x_vals.length
    raise "Dimensions don't match" unless x_len == y_vals.length
    raise 'Not enough data points' if x_len < 3
    self
  end

  def x_range
    (x_vals.first..x_vals.last)
  end

  def min
    y_min_idx, y_min = y_vals.min_with_index
    [x_vals[y_min_idx], y_min]
  end

  def guess_min
    par = to_parabola
    par_opt = par.opt[0]
    x_range.cover?(par_opt) && par.dal? ? par_opt : min[0]
  end

  def to_parabola
    Parabola.new(*Math.regress(self, 2))
  end
end
