# frozen_string_literal: true

# struct for parabola
Parabola = Struct.new(:coef_a, :coef_b, :coef_c) do
  def dal?
    coefficients[0].positive?
  end

  def opt
    [x_opt, y_opt]
  end

  def x_opt
    -coef_b.to_f / (2 * coef_a.to_f)
  end

  def y_opt
    val x_opt
  end

  def coefficients
    to_a
  end

  def val(x_val)
    coef_a.to_f * x_val**2 + coef_b.to_f * x_val + coef_c.to_f
  end
end
