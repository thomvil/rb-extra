# frozen_string_literal: true

module CoreExtensions
  module Array
    # Common helpers for Arrays
    module ArrayExtra
      def partial_match?(some_hash)
        any? { |el| el.partial_match? some_hash }
      end

      def f_replace_at(idx:, el:)
        res = dup
        res[idx] = el
        res
      end

      def cart_prod
        head, tail = arrayify.head_tail
        tail.reduce(head) { |acc, el| acc.product el }.map(&:flatten)
      end

      def pow(degree)
        map { |el| el.to_f**degree }
      end

      def arrayify
        map { |el| [el].flatten }
      end

      def head_tail
        [(self[0] || []), (self[1..-1] || [])]
      end

      def increasing?
        each_cons(2).reduce(true) { |acc, (left, right)| acc && left <= right }
      end

      def decreasing?
        each_cons(2).reduce(true) { |acc, (left, right)| acc && left >= right }
      end

      def monotonic?
        increasing? || decreasing?
      end

      def min_with_index
        head, tail = head_tail
        tail.each_with_index.reduce([0, head]) { |(min_idx, min_val), (el, idx)|
          el < min_val ? [idx + 1, el] : [min_idx, min_val]
        }
      end
    end
  end
end

Array.include CoreExtensions::Array::ArrayExtra
