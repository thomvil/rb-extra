# frozen_string_literal: true

require 'open3'

# :reek:UtilityFunction
module CoreExtensions
  module Object
    # Helpers for executing commands
    module CmdExtra
      def output?(cmd:, cmp_regex:)
        out_stdout, out_stderr, _out_status = Open3.capture3(cmd)
        out_stdout.match?(cmp_regex) || out_stderr.match?(cmp_regex)
      end

      def stdout(cmd)
        out_stdout, _out_stderr, _out_status = Open3.capture3(cmd)
        out_stdout
      end

      def stderr(cmd)
        _out_stdout, out_stderr, _out_status = Open3.capture3(cmd)
        out_stderr
      end

      def run_cmd(cmd_str)
        _stdout, _stderr, status = Open3.capture3(cmd_str)
        status
      end
    end
  end
end

Object.include CoreExtensions::Object::CmdExtra
