# frozen_string_literal: true

require 'oj'
require 'oj_mimic_json'

# :reek:UtilityFunction
module CoreExtensions
  module Object
    # Helpers for (de)serialization
    module SerdeExtra
      def read_json(fname:, default:)
        return default unless File.exist? fname
        JSON.parse File.open(fname).read
      rescue JSON::ParserError
        default
      end

      def save_json(fname:, json_obj:)
        File.open(fname, 'w') { |fh| fh.puts JSON.pretty_generate(json_obj) }
      end
    end
  end
end

Object.include CoreExtensions::Object::SerdeExtra
