# frozen_string_literal: true

# :reek:UtilityFunction
module CoreExtensions
  module Object
    # helpers for IO
    module IoExtra
      def filesize(list_of_files)
        list_of_files.reduce(0) { |acc, fname| acc + File.size(fname) }
      end

      def clear_temp!(mask:)
        tempfiles(mask).each { |fname| File.delete fname }
      end

      def tempfiles(mask = '*')
        Dir.glob File.join('/tmp', mask)
      end

      # :reek:DuplicateMethodCall
      def clean_tmp_env(mask)
        clear_temp! mask: mask
        res = yield
        clear_temp! mask: mask
        res
      end
    end
  end
end

Object.include CoreExtensions::Object::IoExtra
