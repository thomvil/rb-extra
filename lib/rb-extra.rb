# frozen_string_literal: true

require 'awesome_print'
require 'oj'
require 'oj_mimic_json'

Dir[File.join File.dirname(__FILE__), '**', '*.rb']
  .reject { |rb| rb == __FILE__ }
  .each { |rb| require rb }
