# frozen_string_literal: true

version = File.read(File.expand_path('VERSION', __dir__)).strip

Gem::Specification.new do |s|
  s.name        = 'rb-extra'
  s.version     = version
  s.date        = '2018-02-20'
  s.summary     = 'Ruby Extra'
  s.description = 'Frequently used core extensions'
  s.authors     = ['Thomas Villa']
  s.email       = 'thomvil87@gmail.com'
  s.files       = Dir['lib/**/*.rb'] + [
    '.rubocop.yml',
    'Gemfile',
    'rb-extra.gemspec',
    'VERSION'
  ]
  s.homepage    = 'http://gitlab.org/thomvil/rb-extra'
  s.license     = 'MIT'

  s.required_ruby_version     = '>= 2.5.0'
  s.required_rubygems_version = '>= 2.0.0'

  s.add_dependency 'awesome_print', '~> 1.8'
  s.add_dependency 'oj',            '~> 3.4'
  s.add_dependency 'oj_mimic_json', '~> 1.0'
end
